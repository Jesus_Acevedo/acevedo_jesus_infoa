Module 104 Réferencement des sites.
Jesus Acevedo INFOA1
---
* Vous devez installer les logiciels suivants pour pouvoir lancer mon projet. 
UWAMP (https://www.uwamp.com/file/UwAmp.exe) si problèmes (https://www.uwamp.com/fr/?page=download)

* Si impossible à faire fonctionner, télécharger le zip et décompresser-le dans un répertoire. Puis double-clic sur UwAmp.exe (https://www.uwamp.com/file/UwAmp.zip)

* Python : https://www.python.org/downloads/
* Pycharm : https://www.jetbrains.com/fr-fr/student/

* Dès que tout ça est installé, on peut procéder au lancement du projet.

* Pour pouvoir accéder à mon projet ils vous suffit de télécharger le répertoire de mon gitlab ou le lancer si vous l'avez en local.
Lien de mon GITLAB : https://gitlab.com/Jesus_Acevedo/acevedo_jesus_infoa

* Lancez Pycharm. Cliquez sur GET FROM CVS, Dans "URL" mettez mon lien GITLAB et sélectionnez un endroit où vous voulez le sauvegarde.
* Quand cela est fait cliquez sur Clone et Pycharm téléchargera mon projet.

* Quand l'installation sera fini Pycharm vous proposera d'installer un Python Interpreter, Cliquez Oui.

* Quand cela est fait on est prêt pour lancer mon projet.
* Démarrer le serveur MySql (uwamp ou xamp ou mamp, etc)
* Dans PyCharm, importer la BD grâce à un "run" du fichier "zzzdemos/1_ImportationDumpSql.py".
* Puis dans le répertoire racine du projet, ouvrir le fichier "1_run_server_flask.py" et faire un "run".
* Un terminal s'ouvrira et chargera le projet. Dans le terminal de PyCharm un lien pour accèder à mon site est visible.
SITE : http://127.0.0.1:5005/

* Maitenant vous avez accès à ma Base de données. Vous pouvez créer, modifer, supprimer des données dans les tables qui vont sont proposé.
















