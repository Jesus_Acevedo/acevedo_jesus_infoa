"""
    Fichier : gestion_emplacements_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les emplacements.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.emplacements.gestion_emplacements_wtf_forms import FormWTFAjouterEmplacements
from APP_FILMS.emplacements.gestion_emplacements_wtf_forms import FormWTFDeleteEmplacements
from APP_FILMS.emplacements.gestion_emplacements_wtf_forms import FormWTFUpdateEmplacements
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /emplacements_afficher

    Test : ex : http://127.0.0.1:5005/emplacements_afficher

    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_emplacements_sel = 0 >> tous les emplacements.
                id_emplacements_sel = "n" affiche le emplacements dont l'id est "n"
"""


@obj_mon_application.route("/emplacements_afficher/<string:order_by>/<int:id_emplacements_sel>",
                           methods=['GET', 'POST'])
def emplacements_afficher(order_by, id_emplacements_sel):
    print("Emplacement afficher")
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion emplacements ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion emplacements {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_emplacements_sel == 0:
                    strsql_emplacements_afficher = """SELECT id_emplacements, nom_emplacements, adresse_emplacements,
                     commentaires_emplacements, photo_emplacements, date_emplacements FROM t_emplacements WHERE 1"""
                    mc_afficher.execute(strsql_emplacements_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_emplacements"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du emplacements sélectionné avec un nom de variable
                    valeur_id_emplacements_selected_dictionnaire = {
                        "value_id_emplacements_selected": id_emplacements_sel}
                    strsql_emplacements_afficher = """SELECT id_emplacements, nom_emplacements, adresse_emplacements,
                     photo_emplacements, date_emplacements, commentaires_emplacements FROM t_emplacements WHERE id_emplacements = %(value_id_emplacements_selected)s"""

                    mc_afficher.execute(strsql_emplacements_afficher, valeur_id_emplacements_selected_dictionnaire)
                else:
                    strsql_emplacements_afficher = """SELECT id_emplacements, nom_emplacements, adresse_emplacements,
                    commentaires_emplacements photo_emplacements, date_emplacements, commentaires_emplacements FROM t_emplacements ORDER BY id_emplacements DESC"""

                    mc_afficher.execute(strsql_emplacements_afficher)

                data_emplacements = mc_afficher.fetchall()

                print("data_emplacements ", data_emplacements, " Type : ", type(data_emplacements))

                # Différencier les messages si la table est vide.
                if not data_emplacements and id_emplacements_sel == 0:
                    flash("""La table "t_emplacements" est vide. !!""", "warning")
                elif not data_emplacements and id_emplacements_sel > 0:
                    # Si l'utilisateur change l'id_emplacements dans l'URL et que le emplacements n'existe pas,
                    flash(f"Le emplacements demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_emplacements" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données emplacements affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. emplacements_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} emplacements_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("emplacements/emplacements_afficher.html", data=data_emplacements)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /emplacements_ajouter

    Test : ex : http://127.0.0.1:5005/emplacements_ajouter

    Paramètres : sans

    But : Ajouter un emplacements pour un film

    Remarque :  Dans le champ "name_emplacements_html" du formulaire "emplacements/emplacements_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/emplacements_ajouter", methods=['GET', 'POST'])
def emplacements_ajouter_wtf():
    form = FormWTFAjouterEmplacements()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion emplacements ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion emplacements {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():

                nom_emplacements_wtf = form.nom_emplacements_wtf.data
                nom_emplacements = nom_emplacements_wtf.capitalize()

                adresse_emplacements_wtf = form.adresse_emplacements_wtf.data
                adresse_emplacements = adresse_emplacements_wtf.capitalize()

                commentaires_emplacements_wtf = form.commentaires_emplacements_wtf.data
                commentaires_emplacements = commentaires_emplacements_wtf.capitalize()

                date_emplacements_wtf = form.date_emplacements_wtf.data
                date_emplacements = date_emplacements_wtf.capitalize()





                valeurs_insertion_dictionnaire = {"value_nom_emplacements": nom_emplacements,
                                                  "value_adresse_emplacements": adresse_emplacements,
                                                  "value_commentaires_emplacements": commentaires_emplacements,
                                                  "value_date_emplacements": date_emplacements
                                                  }

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_emplacements = """INSERT INTO t_emplacements (id_emplacements,nom_emplacements,adresse_emplacements,commentaires_emplacements,date_emplacements) VALUES (NULL,%(value_nom_emplacements)s,%(value_adresse_emplacements)s,%(value_commentaires_emplacements)s,%(value_date_emplacements)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_emplacements, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('emplacements_afficher', order_by='DESC', id_emplacements_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_emplacements_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_emplacements_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_empl_crud:
            code, msg = erreur_gest_empl_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion emplacements CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_empl_crud.args[0]} , "
                  f"{erreur_gest_empl_crud}", "danger")

    return render_template("emplacements/emplacements_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /emplacements_update

    Test : ex cliquer sur le menu "emplacements" puis cliquer sur le bouton "EDIT" d'un "emplacements"

    Paramètres : sans

    But : Editer(update) un emplacements qui a été sélectionné dans le formulaire "emplacements_afficher.html"

    Remarque :  Dans le champ "nom_emplacements_update_wtf" du formulaire "emplacements/emplacements_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/emplacements_update", methods=['GET', 'POST'])
def emplacements_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_emplacements"
    id_emplacements_update = request.values['id_emplacements_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateEmplacements()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "emplacements_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            nom_emplacements_update = form_update.nom_emplacements_update_wtf.data
            nom_emplacements_update = nom_emplacements_update

            valeur_update_dictionnaire = {"value_id_emplacements": id_emplacements_update,
                                          "value_nom_emplacements": nom_emplacements_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_nom_emplacements = """UPDATE t_emplacements SET nom_emplacements = %(value_nom_emplacements)s WHERE id_emplacements = %(value_id_emplacements)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_nom_emplacements, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_emplacements_update"
            return redirect(
                url_for('emplacements_afficher', order_by="ASC", id_emplacements_sel=id_emplacements_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_emplacements" et "noms" de la "t_emplacements"
            str_sql_id_emplacements = "SELECT id_emplacements, nom_emplacements FROM t_emplacements WHERE id_emplacements = %(value_id_emplacements)s"
            valeur_select_dictionnaire = {"value_id_emplacements": id_emplacements_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_emplacements, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom emplacements" pour l'UPDATE
            data_nom_emplacement = mybd_curseur.fetchone()
            print("data_nom_emplacement ", data_nom_emplacement, " type ", type(data_nom_emplacement), " emplacements ",
                  data_nom_emplacement["nom_emplacements"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "emplacements_update_wtf.html"
            form_update.nom_emplacements_update_wtf.data = data_nom_emplacement["nom_emplacements"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans emplacements_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans emplacements_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")
        flash(f"Erreur dans emplacements_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")
        flash(f"__KeyError dans emplacements_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("emplacements/emplacements_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /emplacements_delete

    Test : ex. cliquer sur le menu "emplacements" puis cliquer sur le bouton "DELETE" d'un "emplacements"

    Paramètres : sans

    But : Effacer(delete) un emplacements qui a été sélectionné dans le formulaire "emplacements_afficher.html"

    Remarque :  Dans le champ "nom_emplacements_delete_wtf" du formulaire "emplacements/emplacements_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/emplacements_delete", methods=['GET', 'POST'])
def emplacements_delete_wtf():
    data_emplacements_attribue_emplacements_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_emplacements"
    id_emplacements_delete = request.values['id_emplacements_btn_delete_html']

    # Objet formulaire pour effacer le emplacements sélectionné.
    form_delete = FormWTFDeleteEmplacements()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("emplacements_afficher", order_by="ASC", id_emplacements_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_materiel_attribue_emplacements_delete = session['data_materiel_attribue_emplacements_delete']
                print("data_materiel_attribue_emplacements_delete ", data_materiel_attribue_emplacements_delete)

                flash(f"Effacer le materiel de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer materiel" qui va irrémédiablement EFFACER le materiel
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_emplacements": id_emplacements_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_materiel_emplacements = """DELETE FROM t_avoir_materiel WHERE fk_emplacements = %(value_id_emplacements)s"""
                str_sql_delete_id_emplacements = """DELETE FROM t_emplacements WHERE id_emplacements = %(value_id_emplacements)s"""
                # Manière brutale d'effacer d'abord la "fk_materiel", même si elle n'existe pas dans la "t_materiel_film"
                # Ensuite on peut effacer le materiel vu qu'il n'est plus "lié" (INNODB) dans la "t_materiel_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_materiel_emplacements, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_emplacements, valeur_delete_dictionnaire)

                flash(f"materiel définitivement effacé !!", "success")
                print(f"materiel définitivement effacé !!")

                # afficher les données
                return redirect(url_for('emplacements_afficher', order_by="ASC", id_emplacements_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_emplacements": id_emplacements_delete}
            print(id_emplacements_delete, type(id_emplacements_delete))

            # Requête qui affiche tous les films qui ont le materiel que l'utilisateur veut effacer
            str_sql_materiel_emplacements_delete = """SELECT id_avoir_materiel, type_materiel, id_emplacements, nom_emplacements FROM t_avoir_materiel
                                            INNER JOIN t_materiel ON t_avoir_materiel.fk_materiel = t_materiel.id_materiel
                                            INNER JOIN t_emplacements ON t_avoir_materiel.fk_emplacements = t_emplacements.id_emplacements
                                            WHERE fk_emplacements = %(value_id_emplacements)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_materiel_emplacements_delete, valeur_select_dictionnaire)
            data_materiel_attribue_emplacements_delete = mybd_curseur.fetchall()
            print("data_materiel_attribue_emplacements_delete...", data_materiel_attribue_emplacements_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_materiel_attribue_emplacements_delete'] = data_materiel_attribue_emplacements_delete

            # Opération sur la BD pour récupérer "id_materiel" et "type_materiel" de la "t_materiel"
            str_sql_id_emplacements = "SELECT id_emplacements, nom_emplacements FROM t_emplacements WHERE id_emplacements = %(value_id_emplacements)s"

            mybd_curseur.execute(str_sql_id_emplacements, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom materiel" pour l'action DELETE
            data_nom_emplacements = mybd_curseur.fetchone()
            print("data_nom_emplacements ", data_nom_emplacements, " type ", type(data_nom_emplacements),
                  " emplacements ",
                  data_nom_emplacements["nom_emplacements"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiel_delete_wtf.html"
            form_delete.nom_emplacements_delete_wtf.data = data_nom_emplacements["nom_emplacements"]

            # Le bouton pour l'action "DELETE" dans le form. "materiel_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans emplacements_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans emplacements_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")

        flash(f"Erreur dans emplacements_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")

        flash(f"__KeyError dans emplacements_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("emplacements/emplacements_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_emplacements_associes=data_materiel_attribue_emplacements_delete)
