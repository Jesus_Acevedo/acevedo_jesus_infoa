"""
    Fichier : gestion_emplacements_materiel_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les emplacements et les materiel.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.emplacements_materiel.gestion_emplacements_materiel_wtf_forms import FormWTFAjouterEmplacementsMateriel
from APP_FILMS.emplacements_materiel.gestion_emplacements_materiel_wtf_forms import FormWTFDeleteEmplacementsMateriel
from APP_FILMS.emplacements_materiel.gestion_emplacements_materiel_wtf_forms import FormWTFUpdateEmplacementsMateriel
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Nom : emplacements_materiel_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /emplacements_materiel_afficher

    But : Afficher les emplacements avec les materiel associés pour chaque emplacements.

    Paramètres : id_materiel_sel = 0 >> tous les emplacements.
                 id_materiel_sel = "n" affiche le emplacements dont l'id est "n"

"""


@obj_mon_application.route("/emplacements_materiel_afficher/<int:id_emplacements_sel>", methods=['GET', 'POST'])
def emplacements_materiel_afficher(id_emplacements_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_emplacements_materiel_afficher:
                code, msg = Exception_init_emplacements_materiel_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_emplacements_materiel_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_emplacements_materiel_afficher.args[0]} , "
                      f"{Exception_init_emplacements_materiel_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_materiel_emplacements_afficher_data = """SELECT id_emplacements, nom_emplacements, adresse_emplacements, commentaires_emplacements, photo_emplacements, date_emplacements,
                                                            GROUP_CONCAT(type_materiel) as MaterielEmplacements FROM t_avoir_materiel
                                                            RIGHT JOIN t_emplacements ON t_emplacements.id_emplacements = t_avoir_materiel.fk_emplacements
                                                            LEFT JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                                            GROUP BY id_emplacements"""
                if id_emplacements_sel == 0:
                    # le paramètre 0 permet d'afficher tous les emplacements
                    # Sinon le paramètre représente la valeur de l'id du emplacements
                    mc_afficher.execute(strsql_materiel_emplacements_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du emplacements sélectionné avec un nom de variable
                    valeur_id_emplacements_selected_dictionnaire = {
                        "value_id_emplacements_selected": id_emplacements_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_materiel_emplacements_afficher_data += """ HAVING id_emplacements= %(value_id_emplacements_selected)s"""

                    mc_afficher.execute(strsql_materiel_emplacements_afficher_data,
                                        valeur_id_emplacements_selected_dictionnaire)

                # Récupère les données de la requête.
                data_materiel_emplacements_afficher = mc_afficher.fetchall()
                print("data_materiel ", data_materiel_emplacements_afficher, " Type : ",
                      type(data_materiel_emplacements_afficher))

                # Différencier les messages.
                if not data_materiel_emplacements_afficher and id_emplacements_sel == 0:
                    flash("""La table "t_emplacements" est vide. !""", "warning")
                elif not data_materiel_emplacements_afficher and id_emplacements_sel > 0:
                    # Si l'utilisateur change l'id_emplacements dans l'URL et qu'il ne correspond à aucun emplacements
                    flash(f"Le emplacements {id_emplacements_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données emplacements et materiel affichés !!", "success")

        except Exception as Exception_emplacements_materiel_afficher:
            code, msg = Exception_emplacements_materiel_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception emplacements_materiel_afficher : {sys.exc_info()[0]} "
                  f"{Exception_emplacements_materiel_afficher.args[0]} , "
                  f"{Exception_emplacements_materiel_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("emplacements_materiel/emplacements_materiel_afficher.html",
                           data=data_materiel_emplacements_afficher)


"""
    nom: edit_materiel_emplacements_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les materiel du emplacements sélectionné par le bouton "MODIFIER" de "emplacements_materiel_afficher.html"

    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les materiel contenus dans la "t_materiel".
    2) Les materiel attribués au emplacements selectionné.
    3) Les materiel non-attribués au emplacements sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_materiel_emplacements_selected", methods=['GET', 'POST'])
def edit_materiel_emplacements_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_materiel_afficher = """SELECT id_materiel, type_materiel FROM t_materiel ORDER BY id_materiel ASC"""
                mc_afficher.execute(strsql_materiel_afficher)
            data_materiel_all = mc_afficher.fetchall()
            print("dans edit_materiel_emplacements_selected ---> data_materiel_all", data_materiel_all)

            # Récupère la valeur de "id_emplacements" du formulaire html "emplacements_materiel_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_emplacements"
            # grâce à la variable "id_emplacements_materiel_edit_html" dans le fichier "emplacements_materiel_afficher.html"
            # href="{{ url_for('edit_materiel_emplacements_selected', id_emplacements_materiel_edit_html=row.id_emplacements) }}"
            id_emplacements_materiel_edit = request.values['id_emplacements_materiel_edit_html']

            # Mémorise l'id du emplacements dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_emplacements_materiel_edit'] = id_emplacements_materiel_edit

            # Constitution d'un dictionnaire pour associer l'id du emplacements sélectionné avec un nom de variable
            valeur_id_emplacements_selected_dictionnaire = {
                "value_id_emplacements_selected": id_emplacements_materiel_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction materiel_emplacements_afficher_data
            # 1) Sélection du emplacements choisi
            # 2) Sélection des materiel "déjà" attribués pour le emplacements.
            # 3) Sélection des materiel "pas encore" attribués pour le emplacements choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "materiel_emplacements_afficher_data"
            data_materiel_emplacements_selected, data_materiel_emplacements_non_attribues, data_materiel_emplacements_attribues = \
                materiel_emplacements_afficher_data(valeur_id_emplacements_selected_dictionnaire)

            print(data_materiel_emplacements_selected)
            lst_data_emplacements_selected = [item['id_emplacements'] for item in data_materiel_emplacements_selected]
            print("lst_data_emplacements_selected  ", lst_data_emplacements_selected,
                  type(lst_data_emplacements_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiel qui ne sont pas encore sélectionnés.
            lst_data_materiel_emplacements_non_attribues = [item['id_materiel'] for item in
                                                            data_materiel_emplacements_non_attribues]
            session[
                'session_lst_data_materiel_emplacements_non_attribues'] = lst_data_materiel_emplacements_non_attribues
            print("lst_data_materiel_emplacements_non_attribues  ", lst_data_materiel_emplacements_non_attribues,
                  type(lst_data_materiel_emplacements_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiel qui sont déjà sélectionnés.
            lst_data_materiel_emplacements_old_attribues = [item['id_materiel'] for item in
                                                            data_materiel_emplacements_attribues]
            session[
                'session_lst_data_materiel_emplacements_old_attribues'] = lst_data_materiel_emplacements_old_attribues
            print("lst_data_materiel_emplacements_old_attribues  ", lst_data_materiel_emplacements_old_attribues,
                  type(lst_data_materiel_emplacements_old_attribues))

            print(" data data_materiel_emplacements_selected", data_materiel_emplacements_selected, "type ",
                  type(data_materiel_emplacements_selected))
            print(" data data_materiel_emplacements_non_attribues ", data_materiel_emplacements_non_attribues, "type ",
                  type(data_materiel_emplacements_non_attribues))
            print(" data_materiel_emplacements_attribues ", data_materiel_emplacements_attribues, "type ",
                  type(data_materiel_emplacements_attribues))

            # Extrait les valeurs contenues dans la table "t_materiel", colonne "type_materiel"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_materiel
            lst_data_materiel_emplacements_non_attribues = [item['type_materiel'] for item in
                                                            data_materiel_emplacements_non_attribues]
            print("lst_all_materiel gf_edit_materiel_emplacements_selected ",
                  lst_data_materiel_emplacements_non_attribues,
                  type(lst_data_materiel_emplacements_non_attribues))

        except Exception as Exception_edit_materiel_emplacements_selected:
            code, msg = Exception_edit_materiel_emplacements_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_materiel_emplacements_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_materiel_emplacements_selected.args[0]} , "
                  f"{Exception_edit_materiel_emplacements_selected}", "danger")

    return render_template("emplacements_materiel/emplacements_materiel_modifier_tags_dropbox.html",
                           data_materiel=data_materiel_all,
                           data_emplacements_selected=data_materiel_emplacements_selected,
                           data_materiel_attribues=data_materiel_emplacements_attribues,
                           data_materiel_non_attribues=data_materiel_emplacements_non_attribues)


"""
    nom: update_materiel_emplacements_selected

    Récupère la liste de tous les materiel du emplacements sélectionné par le bouton "MODIFIER" de "emplacements_materiel_afficher.html"

    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les materiel contenus dans la "t_materiel".
    2) Les materiel attribués au emplacements selectionné.
    3) Les materiel non-attribués au emplacements sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_materiel_emplacements_selected", methods=['GET', 'POST'])
def update_materiel_emplacements_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du emplacements sélectionné
            id_emplacements_selected = session['session_id_emplacements_materiel_edit']
            print("session['session_id_emplacements_materiel_edit'] ", session['session_id_emplacements_materiel_edit'])

            # Récupère la liste des materiel qui ne sont pas associés au emplacements sélectionné.
            old_lst_data_materiel_emplacements_non_attribues = session[
                'session_lst_data_materiel_emplacements_non_attribues']
            print("old_lst_data_materiel_emplacements_non_attribues ", old_lst_data_materiel_emplacements_non_attribues)

            # Récupère la liste des materiel qui sont associés au emplacements sélectionné.
            old_lst_data_materiel_emplacements_attribues = session[
                'session_lst_data_materiel_emplacements_old_attribues']
            print("old_lst_data_materiel_emplacements_old_attribues ", old_lst_data_materiel_emplacements_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme materiel dans le composant "tags-selector-tagselect"
            # dans le fichier "materiel_emplacements_modifier_tags_dropbox.html"
            new_lst_str_materiel_emplacements = request.form.getlist('name_select_tags')
            print("new_lst_str_materiel_emplacements ", new_lst_str_materiel_emplacements)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_materiel_emplacements_old = list(map(int, new_lst_str_materiel_emplacements))
            print("new_lst_materiel_emplacements ", new_lst_int_materiel_emplacements_old,
                  "type new_lst_materiel_emplacements ",
                  type(new_lst_int_materiel_emplacements_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_materiel" qui doivent être effacés de la table intermédiaire "t_materiel_emplacements".
            lst_diff_materiel_delete_b = list(
                set(old_lst_data_materiel_emplacements_attribues) - set(new_lst_int_materiel_emplacements_old))
            print("lst_diff_materiel_delete_b ", lst_diff_materiel_delete_b)

            # Une liste de "id_materiel" qui doivent être ajoutés à la "t_materiel_emplacements"
            lst_diff_materiel_insert_a = list(
                set(new_lst_int_materiel_emplacements_old) - set(old_lst_data_materiel_emplacements_attribues))
            print("lst_diff_materiel_insert_a ", lst_diff_materiel_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_emplacements"/"id_emplacements" et "fk_materiel"/"id_materiel" dans la "t_materiel_emplacements"
            strsql_insert_avoir_materiel = """INSERT INTO t_avoir_materiel (id_avoir_materiel, fk_materiel, fk_emplacements)
                                                    VALUES (NULL, %(value_fk_materiel)s, %(value_fk_emplacements)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_emplacements" et "id_materiel" dans la "t_materiel_emplacements"
            strsql_delete_materiel_emplacements = """DELETE FROM t_avoir_materiel WHERE fk_materiel = %(value_fk_materiel)s AND fk_emplacements = %(value_fk_emplacements)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le emplacements sélectionné, parcourir la liste des materiel à INSÉRER dans la "t_materiel_emplacements".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_materiel_ins in lst_diff_materiel_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du emplacements sélectionné avec un nom de variable
                    # et "id_materiel_ins" (l'id du materiel dans la liste) associé à une variable.
                    valeurs_emplacements_sel_materiel_sel_dictionnaire = {
                        "value_fk_emplacements": id_emplacements_selected,
                        "value_fk_materiel": id_materiel_ins}

                    mconn_bd.mabd_execute(strsql_insert_avoir_materiel,
                                          valeurs_emplacements_sel_materiel_sel_dictionnaire)

                # Pour le emplacements sélectionné, parcourir la liste des materiel à EFFACER dans la "t_materiel_emplacements".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_materiel_del in lst_diff_materiel_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du emplacements sélectionné avec un nom de variable
                    # et "id_materiel_del" (l'id du materiel dans la liste) associé à une variable.
                    valeurs_emplacements_sel_materiel_sel_dictionnaire = {
                        "value_fk_emplacements": id_emplacements_selected,
                        "value_fk_materiel": id_materiel_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_materiel_emplacements,
                                          valeurs_emplacements_sel_materiel_sel_dictionnaire)

        except Exception as Exception_update_materiel_emplacements_selected:
            code, msg = Exception_update_materiel_emplacements_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_materiel_emplacements_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_materiel_emplacements_selected.args[0]} , "
                  f"{Exception_update_materiel_emplacements_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_materiel_emplacements",
    # on affiche les emplacements et le(urs) materiel(s) associé(s).
    return redirect(url_for('emplacements_materiel_afficher', id_emplacements_sel=id_emplacements_selected))


"""
    nom: materiel_emplacements_afficher_data

    Récupère la liste de tous les materiel du emplacements sélectionné par le bouton "MODIFIER" de "emplacements_materiel_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des materiel, ainsi l'utilisateur voit les materiel à disposition

    On signale les erreurs importantes
"""


def materiel_emplacements_afficher_data(valeur_id_emplacements_selected_dict):
    print("valeur_id_emplacements_selected_dict...", valeur_id_emplacements_selected_dict)
    try:

        strsql_emplacements_selected = """SELECT id_emplacements, nom_emplacements, adresse_emplacements, commentaires_emplacements, photo_emplacements, date_emplacements, GROUP_CONCAT(id_materiel) as MaterielEmplacements FROM t_avoir_materiel
                                        INNER JOIN t_emplacements ON t_emplacements.id_emplacements = t_avoir_materiel.fk_emplacements
                                        INNER JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                        WHERE id_emplacements = %(value_id_emplacements_selected)s"""

        strsql_materiel_emplacements_non_attribues = """SELECT id_materiel, type_materiel FROM t_materiel WHERE id_materiel not in(SELECT id_materiel as idMaterielEmplacements FROM t_avoir_materiel
                                                    INNER JOIN t_emplacements ON t_emplacements.id_emplacements = t_avoir_materiel.fk_emplacements
                                                    INNER JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                                    WHERE id_emplacements = %(value_id_emplacements_selected)s)"""

        strsql_materiel_emplacements_attribues = """SELECT id_emplacements, id_materiel, type_materiel FROM t_avoir_materiel
                                            INNER JOIN t_emplacements ON t_emplacements.id_emplacements = t_avoir_materiel.fk_emplacements
                                            INNER JOIN t_materiel ON t_materiel.id_materiel = t_avoir_materiel.fk_materiel
                                            WHERE id_emplacements = %(value_id_emplacements_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_materiel_emplacements_non_attribues, valeur_id_emplacements_selected_dict)
            # Récupère les données de la requête.
            data_materiel_emplacements_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("materiel_emplacements_afficher_data ----> data_materiel_emplacements_non_attribues ",
                  data_materiel_emplacements_non_attribues,
                  " Type : ",
                  type(data_materiel_emplacements_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_emplacements_selected, valeur_id_emplacements_selected_dict)
            # Récupère les données de la requête.
            data_emplacements_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_emplacements_selected  ", data_emplacements_selected, " Type : ",
                  type(data_emplacements_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_materiel_emplacements_attribues, valeur_id_emplacements_selected_dict)
            # Récupère les données de la requête.
            data_materiel_emplacements_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_materiel_emplacements_attribues ", data_materiel_emplacements_attribues, " Type : ",
                  type(data_materiel_emplacements_attribues))

            # Retourne les données des "SELECT"
            return data_emplacements_selected, data_materiel_emplacements_non_attribues, data_materiel_emplacements_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans materiel_emplacements_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans materiel_emplacements_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_materiel_emplacements_afficher_data:
        code, msg = IntegrityError_materiel_emplacements_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans materiel_emplacements_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_materiel_emplacements_afficher_data.args[0]} , "
              f"{IntegrityError_materiel_emplacements_afficher_data}", "danger")


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /emplacements_materiel_ajouter

    Test : ex : http://127.0.0.1:5005/emplacements_ajouter

    Paramètres : sans

    But : Ajouter un emplacements pour un film

    Remarque :  Dans le champ "name_emplacements_html" du formulaire "emplacements/emplacements_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/emplacements_materiel_ajouter", methods=['GET', 'POST'])
def emplacements_materiel_ajouter_wtf():
    form = FormWTFAjouterEmplacementsMateriel()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion emplacements ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion emplacements {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                nom_emplacements_wtf = form.nom_emplacements_wtf.data
                nom_emplacements = nom_emplacements_wtf.capitalize()

                adresse_emplacements_wtf = form.adresse_emplacements_wtf.data
                adresse_emplacements = adresse_emplacements_wtf.capitalize()

                commentaires_emplacements_wtf = form.commentaires_emplacements_wtf.data
                commentaires_emplacements = commentaires_emplacements_wtf.capitalize()

                date_emplacements_wtf = form.date_emplacements_wtf.data
                date_emplacements = date_emplacements_wtf.capitalize()



                valeurs_insertion_dictionnaire = {"value_nom_emplacements": nom_emplacements,
                                                  "value_adresse_emplacements": adresse_emplacements,
                                                  "value_commentaires_emplacements": commentaires_emplacements,
                                                  "value_date_emplacements": date_emplacements}

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_emplacements = """INSERT INTO t_emplacements (id_emplacements,nom_emplacements,adresse_emplacements,commentaires_emplacements,date_emplacements) VALUES (NULL,%(value_nom_emplacements)s,%(value_adresse_emplacements)s,%(value_commentaires_emplacements)s,%(value_date_emplacements)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_emplacements, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('emplacements_materiel_afficher', order_by='DESC', id_emplacements_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_emplacements_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_emplacements_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_matos_crud:
            code, msg = erreur_gest_matos_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion emplacements CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_matos_crud.args[0]} , "
                  f"{erreur_gest_matos_crud}", "danger")

    return render_template("emplacements_materiel/emplacements_materiel_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /emplacements_update

    Test : ex cliquer sur le menu "emplacements" puis cliquer sur le bouton "EDIT" d'un "emplacements"

    Paramètres : sans

    But : Editer(update) un emplacements qui a été sélectionné dans le formulaire "emplacements_afficher.html"

    Remarque :  Dans le champ "nom_emplacements_update_wtf" du formulaire "emplacements/emplacements_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/emplacements_materiel_update", methods=['GET', 'POST'])
def emplacements_materiel_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_emplacements"
    id_emplacements_update = request.values['id_emplacements_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateEmplacementsMateriel()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "emplacements_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_emplacements_update = form_update.nom_emplacements_update_wtf.data
            name_emplacements_update = name_emplacements_update

            valeur_update_dictionnaire = {"value_id_emplacements": id_emplacements_update,
                                          "value_name_emplacements": name_emplacements_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_nom_emplacements = """UPDATE t_emplacements SET nom_emplacements = %(value_name_emplacements)s WHERE id_emplacements = %(value_id_emplacements)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_nom_emplacements, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_emplacements_update"
            return redirect(
                url_for('emplacements_afficher', order_by="ASC", id_emplacements_sel=id_emplacements_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_emplacements" et "noms" de la "t_emplacements"
            str_sql_id_emplacements = "SELECT id_emplacements, nom_emplacements FROM t_emplacements WHERE id_emplacements = %(value_id_emplacements)s"
            valeur_select_dictionnaire = {"value_id_emplacements": id_emplacements_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_emplacements, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom emplacements" pour l'UPDATE
            data_nom_emplacement = mybd_curseur.fetchone()
            print("data_nom_emplacement ", data_nom_emplacement, " type ", type(data_nom_emplacement), " emplacements ",
                  data_nom_emplacement["nom_emplacements"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "emplacements_update_wtf.html"
            form_update.nom_emplacements_update_wtf.data = data_nom_emplacement["nom_emplacements"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans emplacements_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans emplacements_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")
        flash(f"Erreur dans emplacements_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")
        flash(f"__KeyError dans emplacements_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("emplacements/emplacements_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /emplacements_delete

    Test : ex. cliquer sur le menu "emplacements" puis cliquer sur le bouton "DELETE" d'un "emplacements"

    Paramètres : sans

    But : Effacer(delete) un emplacements qui a été sélectionné dans le formulaire "emplacements_afficher.html"

    Remarque :  Dans le champ "nom_emplacements_delete_wtf" du formulaire "emplacements/emplacements_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/emplacements_materiel_delete", methods=['GET', 'POST'])
def emplacements_materiel_delete_wtf():
    data_emplacements_attribue_emplacements_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_emplacements"
    id_emplacements_delete = request.values['id_emplacements_btn_delete_html']

    # Objet formulaire pour effacer le emplacements sélectionné.
    form_delete = FormWTFDeleteEmplacementsMateriel()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("emplacements_afficher", order_by="ASC", id_emplacements_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_materiel_attribue_emplacements_delete = session['data_materiel_attribue_emplacements_delete']
                print("data_materiel_attribue_emplacements_delete ", data_materiel_attribue_emplacements_delete)

                flash(f"Effacer le materiel de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer materiel" qui va irrémédiablement EFFACER le materiel
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_emplacements": id_emplacements_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_materiel_emplacements = """DELETE FROM t_avoir_materiel WHERE fk_emplacements = %(value_id_emplacements)s"""
                str_sql_delete_id_emplacements = """DELETE FROM t_emplacements WHERE id_emplacements = %(value_id_emplacements)s"""
                # Manière brutale d'effacer d'abord la "fk_materiel", même si elle n'existe pas dans la "t_materiel_film"
                # Ensuite on peut effacer le materiel vu qu'il n'est plus "lié" (INNODB) dans la "t_materiel_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_materiel_emplacements, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_emplacements, valeur_delete_dictionnaire)

                flash(f"materiel définitivement effacé !!", "success")
                print(f"materiel définitivement effacé !!")

                # afficher les données
                return redirect(url_for('emplacements_afficher', order_by="ASC", id_emplacements_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_emplacements": id_emplacements_delete}
            print(id_emplacements_delete, type(id_emplacements_delete))

            # Requête qui affiche tous les films qui ont le materiel que l'utilisateur veut effacer
            str_sql_materiel_emplacements_delete = """SELECT id_avoir_materiel, type_materiel, id_emplacements, nom_emplacements FROM t_avoir_materiel
                                            INNER JOIN t_materiel ON t_avoir_materiel.fk_materiel = t_materiel.id_materiel
                                            INNER JOIN t_emplacements ON t_avoir_materiel.fk_emplacements = t_emplacements.id_emplacements
                                            WHERE fk_emplacements = %(value_id_emplacements)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_materiel_emplacements_delete, valeur_select_dictionnaire)
            data_materiel_attribue_emplacements_delete = mybd_curseur.fetchall()
            print("data_materiel_attribue_emplacements_delete...", data_materiel_attribue_emplacements_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "materiel/materiel_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_materiel_attribue_emplacements_delete'] = data_materiel_attribue_emplacements_delete

            # Opération sur la BD pour récupérer "id_materiel" et "type_materiel" de la "t_materiel"
            str_sql_id_emplacements = "SELECT id_emplacements, nom_emplacements FROM t_emplacements WHERE id_emplacements = %(value_id_emplacements)s"

            mybd_curseur.execute(str_sql_id_emplacements, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom materiel" pour l'action DELETE
            data_nom_emplacements = mybd_curseur.fetchone()
            print("data_nom_emplacements ", data_nom_emplacements, " type ", type(data_nom_emplacements),
                  " emplacements ",
                  data_nom_emplacements["nom_emplacements"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiel_delete_wtf.html"
            form_delete.nom_emplacements_delete_wtf.data = data_nom_emplacements["nom_emplacements"]

            # Le bouton pour l'action "DELETE" dans le form. "materiel_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans emplacements_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans emplacements_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")

        flash(f"Erreur dans emplacements_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")

        flash(f"__KeyError dans emplacements_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("emplacements_materiel/emplacements_materiel_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_emplacements_associes=data_materiel_attribue_emplacements_delete)
