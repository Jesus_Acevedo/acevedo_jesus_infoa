"""
    Fichier : gestion_emplacements_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterEmplacementsMateriel(FlaskForm):
    """
        Dans le formulaire "emplacements_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_emplacements_regexp = ""
    nom_emplacements_wtf = StringField("Nom emplacements", validators=[Length(min=1, max=50, message="min 2 max 50"),
                                                                       Regexp(nom_emplacements_regexp,
                                                                              message="Pas de chiffres, de caractères "
                                                                                      "spéciaux, "
                                                                                      "d'espace à double, de double "
                                                                                      "apostrophe, de double trait union")
                                                                       ])
    adresse_emplacements_regexp = ""
    adresse_emplacements_wtf = StringField("Adresse", validators=[Length(min=1, max=50, message="min 2 max 20"),
                                                                  Regexp(adresse_emplacements_regexp)

                                                                  ])
    commentaires_emplacements_regexp = ""
    commentaires_emplacements_wtf = StringField("Commentaires",
                                                validators=[Length(min=1, max=50, message="min 2 max 20"),
                                                            Regexp(commentaires_emplacements_regexp)

                                                            ])

    photo_emplacements_wtf = StringField("Photo")

    date_emplacements_regexp = ""
    date_emplacements_wtf = StringField("Date d'installation",
                                        validators=[Length(min=2, max=50, message="min 2 max 20"),
                                                    Regexp(date_emplacements_regexp)

                                                    ])
    submit = SubmitField("Enregistrer emplacements")


class FormWTFUpdateEmplacementsMateriel(FlaskForm):
    """
        Dans le formulaire "emplacements_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_emplacements_update_regexp = ""
    nom_emplacements_update_wtf = StringField("Clavioter le emplacements ",
                                              validators=[Length(min=2, max=50, message="min 2 max 50"),
                                                          Regexp(nom_emplacements_update_regexp,
                                                                 message="Pas de chiffres, de "
                                                                         "caractères "
                                                                         "spéciaux, "
                                                                         "d'espace à double, de double "
                                                                         "apostrophe, de double trait "
                                                                         "union")
                                                          ])
    submit = SubmitField("Update emplacements")


class FormWTFDeleteEmplacementsMateriel(FlaskForm):
    """
        Dans le formulaire "emplacements_delete_wtf.html"

        nom_emplacements_delete_wtf : Champ qui reçoit la valeur du emplacements, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "emplacements".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_emplacements".
    """
    nom_emplacements_delete_wtf = StringField("Effacer ce emplacements")
    submit_btn_del = SubmitField("Effacer emplacements")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
