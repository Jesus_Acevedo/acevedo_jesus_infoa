-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUESTS MYSQL
-- Database: ACEVEDO_JESUS_INFO1A_SITES_104_2021

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS ACEVEDO_JESUS_INFO1A_SITES_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS ACEVEDO_JESUS_INFO1A_SITES_104_2021;

-- Utilisation de cette base de donnée

USE ACEVEDO_JESUS_INFO1A_SITES_104_2021;
-- --------------------------------------------------------

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_mail`
--

-- Structure de la table `t_avoir_mail`
--

-- Structure de la table `t_avoir_materiel`
--

-- Structure de la table `t_avoir_materiel`
--

CREATE TABLE `t_avoir_materiel` (
  `id_avoir_materiel` int(11) NOT NULL,
  `fk_emplacements` int(11) NOT NULL,
  `fk_materiel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_avoir_materiel`
--

INSERT INTO `t_avoir_materiel` (`id_avoir_materiel`, `fk_emplacements`, `fk_materiel`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `t_emplacements`
--

CREATE TABLE `t_emplacements` (
  `id_emplacements` int(11) NOT NULL,
  `nom_emplacements` longtext,
  `adresse_emplacements` longtext,
  `commentaires_emplacements` longtext,
  `photo_emplacements` longtext,
  `date_emplacements` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_emplacements`
--

INSERT INTO `t_emplacements` (`id_emplacements`, `nom_emplacements`, `adresse_emplacements`, `commentaires_emplacements`, `photo_emplacements`, `date_emplacements`) VALUES
(1, 'LGF-066-Berne-Papiermühlestrasse 83', 'Papiermühlestrasse 83, Bern', 'L\'installation s\'est passé comme prévue. Fonctionnel !', '\\static\\images\\EMPLACEMENTS\\LGF1.jpg', '2021-05-18'),
(2, 'LGF-029-Vevey-Av. Général-Guisan 31', 'Avenue du Général-Guisan 31, Vevey', 'L\'installation est prévue cette semaine.', '\\static\\images\\EMPLACEMENTS\\LGF2.jpg', '2021-05-14'),
(14, 'LGF-014-Genève-Avenue De Miremont 11', 'Avenue De Miremont 11, Genève', 'Pas encore installé', '\\static\\images\\EMPLACEMENTS\\LGF3.jpg', '2021-09-15'),
(15, 'LGF-044-Cernier-Rue Comble Emine 1', 'Rue Comble Emine 1, Cernier\r\n', 'Pas encore installé', '\\static\\images\\EMPLACEMENTS\\LGF4.jpg', '2022-02-05');

-- --------------------------------------------------------

--
-- Structure de la table `t_emplacements_personnes`
--

CREATE TABLE `t_emplacements_personnes` (
  `id_emplacements_personnes` int(11) NOT NULL,
  `fk_emplacements` int(11) NOT NULL,
  `fk_personnes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_emplacements_personnes`
--

INSERT INTO `t_emplacements_personnes` (`id_emplacements_personnes`, `fk_emplacements`, `fk_personnes`) VALUES
(3, 1, 9),
(10, 14, 9),
(12, 1, 14),
(13, 14, 12);

-- --------------------------------------------------------

--
-- Structure de la table `t_materiel`
--

CREATE TABLE `t_materiel` (
  `id_materiel` int(11) NOT NULL,
  `type_materiel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_materiel`
--

INSERT INTO `t_materiel` (`id_materiel`, `type_materiel`) VALUES
(1, 'Player'),
(2, 'PDU'),
(3, 'rPi'),
(4, 'Switch'),
(9, 'Ibase'),
(10, 'Nuc'),
(11, 'Nexcom');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_personnes` int(11) NOT NULL,
  `nom_personnes` varchar(42) DEFAULT NULL,
  `prenom_personnes` varchar(42) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_personnes`, `nom_personnes`, `prenom_personnes`) VALUES
(9, 'Acevedo', 'Jesus'),
(10, 'Macron', 'Francois'),
(11, 'Bondour', 'Jérome'),
(12, 'Burdy', 'Damien'),
(13, 'Dd', 'André'),
(14, 'Pansky', 'Pascal');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
  ADD PRIMARY KEY (`id_avoir_materiel`),
  ADD KEY `fk_emplacements` (`fk_emplacements`),
  ADD KEY `fk_materiel` (`fk_materiel`);

--
-- Index pour la table `t_emplacements`
--
ALTER TABLE `t_emplacements`
  ADD PRIMARY KEY (`id_emplacements`);

--
-- Index pour la table `t_emplacements_personnes`
--
ALTER TABLE `t_emplacements_personnes`
  ADD PRIMARY KEY (`id_emplacements_personnes`),
  ADD KEY `fk_emplacements` (`fk_emplacements`),
  ADD KEY `fk_materiel` (`fk_personnes`);

--
-- Index pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  ADD PRIMARY KEY (`id_materiel`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_personnes`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
  MODIFY `id_avoir_materiel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `t_emplacements`
--
ALTER TABLE `t_emplacements`
  MODIFY `id_emplacements` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `t_emplacements_personnes`
--
ALTER TABLE `t_emplacements_personnes`
  MODIFY `id_emplacements_personnes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  MODIFY `id_materiel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_personnes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
  ADD CONSTRAINT `t_avoir_materiel_ibfk_1` FOREIGN KEY (`fk_emplacements`) REFERENCES `t_emplacements` (`id_emplacements`),
  ADD CONSTRAINT `t_avoir_materiel_ibfk_2` FOREIGN KEY (`fk_materiel`) REFERENCES `t_materiel` (`id_materiel`);

--
-- Contraintes pour la table `t_emplacements_personnes`
--
ALTER TABLE `t_emplacements_personnes`
  ADD CONSTRAINT `t_emplacements_personnes_ibfk_1` FOREIGN KEY (`fk_emplacements`) REFERENCES `t_emplacements` (`id_emplacements`),
  ADD CONSTRAINT `t_emplacements_personnes_ibfk_2` FOREIGN KEY (`fk_personnes`) REFERENCES `t_personnes` (`id_personnes`);

