"""
    Fichier : gestion_personnes_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les personnes.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.personnes.gestion_personnes_wtf_forms import FormWTFAjouterPersonnes
from APP_FILMS.personnes.gestion_personnes_wtf_forms import FormWTFDeletePersonnes
from APP_FILMS.personnes.gestion_personnes_wtf_forms import FormWTFUpdatePersonnes

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /personnes_afficher

    Test : ex : http://127.0.0.1:5005/personnes_afficher

    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_personnes_sel = 0 >> tous les personnes.
                id_personnes_sel = "n" affiche le personnes dont l'id est "n"
"""


@obj_mon_application.route("/personnes_afficher/<string:order_by>/<int:id_personnes_sel>", methods=['GET', 'POST'])
def personnes_afficher(order_by, id_personnes_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion personnes ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionpersonnes {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_personnes_sel == 0:
                    strsql_personnes_afficher = """SELECT id_personnes, nom_personnes, prenom_personnes FROM t_personnes ORDER BY id_personnes ASC"""
                    mc_afficher.execute(strsql_personnes_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_personnes"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du personnes sélectionné avec un nom de variable
                    valeur_id_personnes_selected_dictionnaire = {"value_id_personnes_selected": id_personnes_sel}
                    strsql_personnes_afficher = """SELECT id_personnes, nom_personnes, prenom_personnes FROM t_personnes WHERE id_personnes = %(value_id_personnes_selected)s"""

                    mc_afficher.execute(strsql_personnes_afficher, valeur_id_personnes_selected_dictionnaire)
                else:
                    strsql_personnes_afficher = """SELECT id_personnes, nom_personnes, prenom_personnes FROM t_personnes ORDER BY id_personnes DESC"""

                    mc_afficher.execute(strsql_personnes_afficher)

                data_personnes = mc_afficher.fetchall()

                print("data_personnes ", data_personnes, " Type : ", type(data_personnes))

                # Différencier les messages si la table est vide.
                if not data_personnes and id_personnes_sel == 0:
                    flash("""La table "t_personnes" est vide. !!""", "warning")
                elif not data_personnes and id_personnes_sel > 0:
                    # Si l'utilisateur change l'id_personnes dans l'URL et que le personnes n'existe pas,
                    flash(f"Le personnes demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_personnes" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données personnes affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. personnes_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} personnes_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("personnes/personnes_afficher.html", data=data_personnes)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /personnes_ajouter

    Test : ex : http://127.0.0.1:5005/personnes_ajouter

    Paramètres : sans

    But : Ajouter un personnes pour un film

    Remarque :  Dans le champ "name_personnes_html" du formulaire "personnes/personnes_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/personnes_ajouter", methods=['GET', 'POST'])
def personnes_ajouter_wtf():
    form = FormWTFAjouterPersonnes()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion personnes ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionpersonnes {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                nom_personnes_wtf = form.nom_personnes_wtf.data
                nom_personnes = nom_personnes_wtf.capitalize()

                prenom_personnes_wtf = form.prenom_personnes_wtf.data
                prenom_personnes = prenom_personnes_wtf.capitalize()

                valeurs_insertion_dictionnaire = {"value_nom_personnes": nom_personnes,
                                                  "value_prenom_personnes": prenom_personnes}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_personnes = """INSERT INTO t_personnes (id_personnes,nom_personnes, prenom_personnes) VALUES (NULL,%(value_nom_personnes)s,%(value_prenom_personnes)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_personnes, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('personnes_afficher', order_by='DESC', id_personnes_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_personnes_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_personnes_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_matos_crud:
            code, msg = erreur_gest_matos_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion personnes CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_matos_crud.args[0]} , "
                  f"{erreur_gest_matos_crud}", "danger")

    return render_template("personnes/personnes_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /personnes_update

    Test : ex cliquer sur le menu "personnes" puis cliquer sur le bouton "EDIT" d'un "personnes"

    Paramètres : sans

    But : Editer(update) un personnes qui a été sélectionné dans le formulaire "personnes_afficher.html"

    Remarque :  Dans le champ "nom_personnes_update_wtf" du formulaire "personnes/personnes_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/personnes_update", methods=['GET', 'POST'])
def personnes_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_personnes"
    id_personnes_update = request.values['id_personnes_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatePersonnes()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "personnes_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            nom_personnes_update = form_update.nom_personnes_update_wtf.data
            nom_personnes_update = nom_personnes_update.capitalize()

            valeur_update_dictionnaire = {"value_id_personnes": id_personnes_update,
                                          "value_nom_personnes": nom_personnes_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_nom_personnes = """UPDATE t_personnes SET nom_personnes = %(value_nom_personnes)s WHERE id_personnes = %(value_id_personnes)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_nom_personnes, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_personnes_update"
            return redirect(url_for('personnes_afficher', order_by="ASC", id_personnes_sel=id_personnes_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_personnes" et "nom_personnes" de la "t_personnes"
            str_sql_id_personnes = "SELECT id_personnes, nom_personnes FROM t_personnes WHERE id_personnes = %(value_id_personnes)s"
            valeur_select_dictionnaire = {"value_id_personnes": id_personnes_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_personnes, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom personnes" pour l'UPDATE
            data_nom_personnes = mybd_curseur.fetchone()
            print("data_nom_personnes ", data_nom_personnes, " type ", type(data_nom_personnes), " personnes ",
                  data_nom_personnes["nom_personnes"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "personnes_update_wtf.html"
            form_update.nom_personnes_update_wtf.data = data_nom_personnes["nom_personnes"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans personnes_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans personnes_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")
        flash(f"Erreur dans personnes_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")
        flash(f"__KeyError dans personnes_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("personnes/personnes_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /personnes_delete

    Test : ex. cliquer sur le menu "personnes" puis cliquer sur le bouton "DELETE" d'un "personnes"

    Paramètres : sans

    But : Effacer(delete) un personnes qui a été sélectionné dans le formulaire "personnes_afficher.html"

    Remarque :  Dans le champ "nom_personnes_delete_wtf" du formulaire "personnes/personnes_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/personnes_delete", methods=['GET', 'POST'])
def personnes_delete_wtf():
    data_emplacements_attribue_personnes_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_personnes"
    id_personnes_delete = request.values['id_personnes_btn_delete_html']

    # Objet formulaire pour effacer le personnes sélectionné.
    form_delete = FormWTFDeletePersonnes()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("personnes_afficher", order_by="ASC", id_personnes_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "personnes/personnes_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_emplacements_attribue_personnes_delete = session['data_emplacements_attribue_personnes_delete']
                print("data_emplacements_attribue_personnes_delete ", data_emplacements_attribue_personnes_delete)

                flash(f"Effacer le personnes de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer personnes" qui va irrémédiablement EFFACER le personnes
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_personnes": id_personnes_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_emplacements_personnes = """DELETE FROM t_emplacements_personnes WHERE fk_personnes = %(value_id_personnes)s"""
                str_sql_delete_id_personnes = """DELETE FROM t_personnes WHERE id_personnes = %(value_id_personnes)s"""
                # Manière brutale d'effacer d'abord la "fk_personnes", même si elle n'existe pas dans la "t_personnes_film"
                # Ensuite on peut effacer le personnes vu qu'il n'est plus "lié" (INNODB) dans la "t_personnes_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_emplacements_personnes, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_personnes, valeur_delete_dictionnaire)

                flash(f"personnes définitivement effacé !!", "success")
                print(f"personnes définitivement effacé !!")

                # afficher les données
                return redirect(url_for('personnes_afficher', order_by="ASC", id_personnes_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_personnes": id_personnes_delete}
            print(id_personnes_delete, type(id_personnes_delete))

            # Requête qui affiche tous les films qui ont le personnes que l'utilisateur veut effacer
            str_sql_personnes_emplacements_delete = """SELECT id_emplacements_personnes, nom_emplacements, id_personnes, nom_personnes FROM t_emplacements_personnes
                                            INNER JOIN t_emplacements ON t_emplacements_personnes.fk_emplacements = t_emplacements.id_emplacements
                                            INNER JOIN t_personnes ON t_emplacements_personnes.fk_personnes = t_personnes.id_personnes
                                            WHERE fk_personnes = %(value_id_personnes)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_personnes_emplacements_delete, valeur_select_dictionnaire)
            data_emplacements_attribue_personnes_delete = mybd_curseur.fetchall()
            print("data_emplacements_attribue_personnes_delete...", data_emplacements_attribue_personnes_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "personnes/personnes_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_emplacements_attribue_personnes_delete'] = data_emplacements_attribue_personnes_delete

            # Opération sur la BD pour récupérer "id_personnes" et "type_personnes" de la "t_personnes"
            str_sql_id_personnes = "SELECT id_personnes, nom_personnes FROM t_personnes WHERE id_personnes = %(value_id_personnes)s"

            mybd_curseur.execute(str_sql_id_personnes, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom materiel" pour l'action DELETE
            data_nom_personnes = mybd_curseur.fetchone()
            print("data_nom_personnes ", data_nom_personnes, " type ", type(data_nom_personnes), " personnes ",
                  data_nom_personnes["nom_personnes"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiel_delete_wtf.html"
            form_delete.nom_personnes_delete_wtf.data = data_nom_personnes["nom_personnes"]

            # Le bouton pour l'action "DELETE" dans le form. "materiel_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans nom_personnes_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans personnes_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_matos_crud:
        code, msg = erreur_gest_matos_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_matos_crud} ", "danger")

        flash(f"Erreur dans personnes_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_matos_crud.args[0]} , "
              f"{erreur_gest_matos_crud}", "danger")

        flash(f"__KeyError dans personnes_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("personnes/personnes_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_emplacements_associes=data_emplacements_attribue_personnes_delete)
