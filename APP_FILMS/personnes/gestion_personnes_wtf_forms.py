"""
    Fichier : gestion_emplacements_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterPersonnes(FlaskForm):
    """
        Dans le formulaire "emplacements_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_personnes_regexp = ""
    nom_personnes_wtf = StringField("Nom de famille de la personne", validators=[Length(min=1, max=50, message="min 2 max 50"),
                                                                       Regexp(nom_personnes_regexp,
                                                                              message="Pas de chiffres, de caractères "
                                                                                      "spéciaux, "
                                                                                      "d'espace à double, de double "
                                                                                      "apostrophe, de double trait union")
                                                                       ])
    prenom_personnes_regexp = ""
    prenom_personnes_wtf = StringField("Prenom personne",
                                              validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                          Regexp(prenom_personnes_regexp,
                                                                 message="Pas de chiffres, de "
                                                                         "caractères "
                                                                         "spéciaux, "
                                                                         "d'espace à double, de double "
                                                                         "apostrophe, de double trait "
                                                                         "union")
                                                          ])

    submit = SubmitField("Ajouter personne")


class FormWTFUpdatePersonnes(FlaskForm):
    """
        Dans le formulaire "emplacements_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_personnes_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_personnes_update_wtf = StringField("Nom de famille de la personne ",
                                              validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                          Regexp(nom_personnes_update_regexp,
                                                                 message="Pas de chiffres, de "
                                                                         "caractères "
                                                                         "spéciaux, "
                                                                         "d'espace à double, de double "
                                                                         "apostrophe, de double trait "
                                                                         "union")
                                                          ])


    submit = SubmitField("Modifier personne")


class FormWTFDeletePersonnes(FlaskForm):
    """
        Dans le formulaire "emplacements_delete_wtf.html"

        nom_personne_delete_wtf : Champ qui reçoit la valeur du emplacements, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "emplacements".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_emplacements".
    """
    nom_personnes_delete_wtf = StringField("Effacer cette personne")
    submit_btn_del = SubmitField("Effacer personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
