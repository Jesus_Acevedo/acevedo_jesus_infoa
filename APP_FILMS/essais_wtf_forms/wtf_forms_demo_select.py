"""
    Fichier : gestion_materiel_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF

    But : Essayer un formulaire avec WTForms
"""
from flask_wtf import FlaskForm
from wtforms import BooleanField
from wtforms import PasswordField
from wtforms import SelectField
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp


class MonPremierWTForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(message="what the form bandes de merde")])

    nom_materiel_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_materiel_wtf = StringField("Clavioter le materiel ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                         Regexp(nom_materiel_regexp,
                                                                                message="Pas de chiffres, de caractères "
                                                                                        "spéciaux, d'espace à double, "
                                                                                        "de double apostrophe, "
                                                                                        "de double trait union")
                                                                         ])

    case_cocher_npc = BooleanField('Ne pas cliquer')

    submit = SubmitField('Ok !')


"""
    Dans le formulaire "templates/zzz_essais_om_104/demo_form_select_wtf.html"
    Auteur : OM 2021.04.11
    
    But : Montrer l'utilisation d'une liste déroulante (WTF) dont le contenu est basé sur la table "t_emplacement"
    
"""


class DemoFormSelectWTF(FlaskForm):
    materiel_dropdown_wtf = SelectField('Materiel (liste déroulante)',
                                        validators=[DataRequired(message="Sélectionner un materiel.")],
                                        validate_choice=False
                                        )
    # Alternative qui correspond aux lignes en commentaires lignes 88 et 89 du "gestion_wtf_forms_demo_select.py"
    # materiel_dropdown_wtf = SelectField('Materiel (liste déroulante)',
    #                                   validators=[DataRequired(message="Sélectionner un materiel.")],
    #                                   validate_choice=False,
    #                                   coerce=int
    #                                   )
    submit_btn_ok_dplist_materiel = SubmitField("Choix materiel")
